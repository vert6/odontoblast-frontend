import React, { useState } from 'react'
import axios from 'axios'
import styled from 'styled-components'
import UserService from '../services/UserService'

import OutlinedInput from '../components/OutlinedInput'
import LogoImage from '../assets/sample-icon.jpg'

const Container = styled.div`
  background-color: #fff;
  display: grid;
  margin: 0;
  padding: 0;
  min-height: 100vh;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  background-image: linear-gradient(
    to right top,
    #6bdfe8,
    #69e7e9,
    #69efe8,
    #6ef7e6,
    #75ffe3
  );
  align-items: center;
  align-content: center;
`

const FormColumn = styled.div`
  box-sizing: border-box;
  width: 80%;
  background-color: white;
  padding: 3em;
  border-radius: 10px;
`

const AdColumn = styled.div`
  box-sizing: border-box;
  text-align: center;
`

const Logo = styled.div`
  background-image: url(${LogoImage});
  background-size: cover;
  background-position-x: -20px;
  height: 72px;
  width: 72px;
`

const LoginForm = styled.form`
  margin: 7em auto auto auto;
  width: 100%;
  height: auto;
  box-sizing: border-box;
`

const FormGroup = styled.div``

const Label = styled.label`
  color: grey;
`

const SubmitButton = styled.button`
  background-image: linear-gradient(
    to right,
    #6bdfe8,
    #69e7e9,
    #69efe8,
    #6ef7e6,
    #75ffe3
  );
  padding: 16px 32px;
  border: none;
  font-size: 16px;
  color: #fff;
  border-radius: 4px;
  outline: 0;
`

const Jumbotron = styled.h1`
  color: white;
  font-size: 56px;
  margin-top: 25vh;
`

const PasswordReset = styled.a`
  text-decoration: underline;
  font-size: 16px;
  display: block;
  margin: 16px 0px;
  color: #8f9396;
  cursor: pointer;
`

const NewPatient = () => {
  
  const [newUser, setNewUser] = useState({
    first_name: '',
    last_name: '',
    email: '',
    number: '',
    password: '',
    password_confirmation: '',
  })
  
  const changeHandler = (e) => {
    setNewUser({...newUser, [e.target.name]: e.target.value})
  } 

  return (
    <Container>
       <AdColumn>
        <Jumbotron>Cadastre um novo Paciente!</Jumbotron>
      </AdColumn>
      <FormColumn>
        {/* <Logo /> */}
        {/*  first_name, last_name, email, number, password, password_confirmation */}
        <LoginForm>
        <FormGroup>
            <Label>Nome</Label>
            <OutlinedInput
              type="first_name"
              name="first_name"
              onChange={changeHandler}
            />
          </FormGroup>
          
          <SubmitButton type="button" onClick={() => UserService.signUp(newUser)}>
            Cadastrar-se
          </SubmitButton>
        </LoginForm>
      </FormColumn>
     
    </Container>
  )
}

export default NewPatient


import React, { useState } from 'react'
import axios from 'axios'
import styled from 'styled-components'
import UserService from '../services/UserService'

import OutlinedInput from '../components/OutlinedInput'
import LogoImage from '../assets/sample-icon.jpg'

const Container = styled.div`
  background-color: #fff;
  display: grid;
  grid-template-columns: 2fr 3fr;
  margin: 0;
  padding: 0;
  height: 100vh;
  box-sizing: border-box;
`

const FormColumn = styled.div`
  height: 100%;
  padding: 0px 24px 0px 24px;
  box-sizing: border-box;
`

const AdColumn = styled.div`
  height: 100%;
  background-image: linear-gradient(
    to right top,
    #6bdfe8,
    #69e7e9,
    #69efe8,
    #6ef7e6,
    #75ffe3
  );
  box-sizing: border-box;
  text-align: center;
`

const Logo = styled.div`
  background-image: url(${LogoImage});
  background-size: cover;
  background-position-x: -20px;
  height: 72px;
  width: 72px;
`

const SignupForm = styled.form`
  margin: 7em auto auto auto;
  width: auto;
  height: auto;
  box-sizing: border-box;
`

const FormGroup = styled.div``

const Label = styled.label`
  color: #ccd2d8;
`

const SubmitButton = styled.button`
  background-image: linear-gradient(
    to right,
    #6bdfe8,
    #69e7e9,
    #69efe8,
    #6ef7e6,
    #75ffe3
  );
  padding: 16px 32px;
  border: none;
  font-size: 16px;
  color: #fff;
  border-radius: 4px;
  outline: 0;
`

const Jumbotron = styled.h1`
  color: white;
  font-size: 72px;
  margin-top: 25vh;
`

const PasswordReset = styled.a`
  text-decoration: underline;
  font-size: 16px;
  display: block;
  margin: 16px 0px;
  color: #8f9396;
  cursor: pointer;
`

const SignUp = () => {
  const [newUser, setNewUser] = useState({
    first_name: '',
    last_name: '',
    email: '',
    number: '',
    password: '',
    password_confirmation: ''
  })

  const changeHandler = e => {
    setNewUser({ ...newUser, [e.target.name]: e.target.value })
  }

  const submitForm = async () => {
    const res = await UserService.signUp(newUser)
    console.log(res)
  }

  return (
    <Container>
      <AdColumn>
        <Jumbotron>Cadastre-se!</Jumbotron>
      </AdColumn>
      <FormColumn>
        {/* <Logo /> */}
        {/*  first_name, last_name, email, number, password, password_confirmation */}
        <SignupForm>
          <FormGroup>
            <Label>Nome</Label>
            <OutlinedInput
              type="first_name"
              name="first_name"
              onChange={changeHandler}
            />
          </FormGroup>
          <FormGroup>
            <Label>Sobrenome</Label>
            <OutlinedInput
              type="last_name"
              name="last_name"
              onChange={changeHandler}
            />
          </FormGroup>
          <FormGroup>
            <Label>Email</Label>
            <OutlinedInput type="email" name="email" onChange={changeHandler} />
          </FormGroup>
          <FormGroup>
            <Label>Telefone</Label>
            <OutlinedInput
              type="number"
              name="number"
              onChange={changeHandler}
            />
          </FormGroup>
          <FormGroup>
            <Label>Senha</Label>
            <OutlinedInput
              type="password"
              name="password"
              onChange={changeHandler}
            />
          </FormGroup>
          <FormGroup>
            <Label>Confirme a sua senha</Label>
            <OutlinedInput
              type="password"
              name="password_confirmation"
              onChange={changeHandler}
            />
          </FormGroup>
          <SubmitButton type="button" onClick={() => submitForm()}>
            Cadastrar-se
          </SubmitButton>
        </SignupForm>
      </FormColumn>
    </Container>
  )
}

export default SignUp

import React, { useState, useEffect, useRef } from 'react'
import PatientService from '../services/PatientService'
import styled from 'styled-components'
import spinner from '../assets/loading.gif'

import OutlinedInput from '../components/OutlinedInput'
import LogoImage from '../assets/sample-icon.jpg'

const Container = styled.div`
    width: 100%;
    padding: 100px;
    min-height: 100vh;  
    box-sizing: border-box;
    display: flex;
    flex-direction: column;
    background-image: linear-gradient(
    to bottom,
    #6bdfe8,
    #69e7e9,
    #69efe8,
    #6ef7e6,
    #75ffe3
  );
  main {
    display: flex;
    justify-content: space-around;
  }

`

const InfoBar = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    box-sizing: border-box;
    text-align: center;
    margin-bottom: 3em;
    background-color: #EDEFFF; 
    padding: 0.4em;    
    align-items: center;
`

const Card = styled.div`
    background-color: #EDEFFF; 
    width: 40vw;
    min-height: 50vh;
    border-radius: 10px;
    padding: 0.8em;
    box-sizing: border-box;
    h3 {
        font-size: 1.5em;
        color: #6bdfe8;
        margin-top: 0;
    }
    &.pacientes {
        ul {
            overflow-y: scroll;
        }
    }
    &.info {
        width: 250px;
        height: 80px;
        min-height: 80px;
        font-size: 1.4em;
        color: grey;
        background-color: transparent;
        em {
            font-style: normal;
            font-weight: bolder;
            color: #6bdfe8;
        }
    }
`
const Button = styled.button`
  background-image: linear-gradient(
    to right,
    #6bdfe8,
    #69e7e9,
    #69efe8
  );
  padding: 16px 32px;
  border: none;
  font-size: 16px;
  height: 3em;
  width: auto;
  color: #fff;
  border-radius: 4px;
  outline: 0;
`

const List = styled.ul`
    margin: 0;
    padding: 0;
    li {   
        margin: 0;
        margin-bottom: 0.5em;
        display: flex;
        flex-flow: column; 
        padding-bottom: 0.5em;
        border-bottom: solid 2px #6bdfe8;
        div {
            display: flex;
            justify-content: space-between;
            flex-flow: row;
            color: #3e6765;
            h4 {
                font-size: 1em;
                margin: 0;
            }
            time {
                font-size: 1em;
            }
        }
        em {
            font-size: 0.7em;
            color: grey;
            margin: 0;
            font-style: normal;
        }
    }

`

const Spinner = styled.img`
    width: 3em;
`

const CardList = ({title, type, data}) => {
    let mappedList
    const {items} = data
    if(! data.isFetching) {
        if(type === 'agendamentos'){
            mappedList = items.map( item => (
            <li>
                <div>
                    <h4>{item.pacientName}</h4>
                    <time>{item.time}</time>
                </div>
                <em>{item.type}</em>
            </li>) )
        } else {
            mappedList = items.map( item => (
                <li>
                    <div>
                        <h4>{item.name}</h4>
                    </div>
                    <em>{item.birthDate}</em>
                </li>) )
        }
    }

    return(
        <Card className={type}>
            <h3>{title}</h3>
            {data.isFetching
            ? <Spinner src={spinner} /> 
            : <List>{mappedList}</List>
         }
        </Card>
    )
}

const mapPatientsResponse = (data) => data.map(item => item)

const Dashboard = () => {

    const [Patients, setPatients] = useState({
        isFetching: true, 
        items: []
    }) 

    const firstLoading = useRef(true)

    useEffect(() => {
        if (firstLoading.current) {
            firstLoading.current = false
            PatientService.getAll()
            .then( data => {
                setPatients({
                    isLoading: false,
                    items: mapPatientsResponse(data)
                })
            })
        }
    })
    

    const agenda = {
        isFetching: true,
        items: [
        {
            pacientName: 'Fulano',
            time: '14:00',
            type: 'consulta'
        },
        {
            pacientName: 'Fulano',
            time: '14:00',
            type: 'consulta'
        },
        {
            pacientName: 'Fulano',
            time: '14:00',
            type: 'consulta'
        },
        {
            pacientName: 'Fulano',
            time: '14:00',
            type: 'consulta'
        },
    ]}

    const pacientes = {
        isFetching: true,
        items :[
        {
            name: 'Fulano',
            birthDate: '10/02/1980'
        },
        {
            name: 'Fulano de Trajanos',
            birthDate: '10/02/1980'
        },
        {
            name: 'Acássio Desterro Filho',
            birthDate: '10/02/1980'
        }
    ]}

    
    return  (
        <Container>
           <InfoBar>
               <Card className='info'>
                { 
                    Patients.isFetching ? 
                    <Spinner src={spinner} />
                    : <p>Pacientes: <em>{Patients.items.length}</em></p> 
                }
                   
               </Card>
               <Card className='info'>
                   Agendamentos: <em>10</em>
               </Card>
               <Button>
                   Adicionar Paciente
               </Button>
           </InfoBar>
           <main>
            <CardList 
            title='Agendamentos Hoje'
            type='agendamentos'
            data={agenda} />
            <CardList 
            title='Seus Pacientes'
            type='pacientes'
            data={Patients} />
           </main>
        </Container>
    )
}

export default Dashboard
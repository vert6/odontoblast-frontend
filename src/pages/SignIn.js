import React, { useState } from 'react'
import UserService from '../services/UserService'
import styled from 'styled-components'

import OutlinedInput from '../components/OutlinedInput'
import LogoImage from '../assets/sample-icon.jpg'

const Container = styled.div`
  background-color: #fff;
  display: grid;
  grid-template-columns: 1fr 2fr;
  margin: 0;
  padding: 0;
  height: 100vh;
`

const FormColumn = styled.div`
  height: 100%;
  padding: 0px 24px 0px 24px;
`

const AdColumn = styled.div`
  height: 100%;
  background-image: linear-gradient(
    to right top,
    #6bdfe8,
    #69e7e9,
    #69efe8,
    #6ef7e6,
    #75ffe3
  );
  text-align: center;
`

const Logo = styled.div`
  background-image: url(${LogoImage});
  background-size: cover;
  background-position-x: -20px;
  height: 72px;
  width: 72px;
`

const LoginForm = styled.form`
  margin: 30% auto auto auto;
  width: 80%;
`

const FormGroup = styled.div``

const Label = styled.label`
  color: #ccd2d8;
`

const LoginButton = styled.button`
  background-image: linear-gradient(
    to right,
    #6bdfe8,
    #69e7e9,
    #69efe8,
    #6ef7e6,
    #75ffe3
  );
  padding: 16px 32px;
  border: none;
  font-size: 16px;
  color: #fff;
  border-radius: 4px;
  outline: 0;
`

const Jumbotron = styled.h1`
  color: white;
  font-size: 72px;
  margin-top: 25vh;
`

const PasswordReset = styled.a`
  text-decoration: underline;
  font-size: 16px;
  display: block;
  margin: 16px 0px;
  color: #8f9396;
  cursor: pointer;
`

const SignIn = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  async function signIn() {
    UserService.signIn(email, password)
    // axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'
    // try {
    //   const response = await axios.post('http://localhost:5000/signin')
    //   console.log(response)
    // } catch (error) {
    //   console.error(error)
    // }
  }

  return (
    <Container>
      <FormColumn>
        {/* <Logo /> */}
        <LoginForm>
          <FormGroup>
            <Label>Email</Label>
            <OutlinedInput
              type="email"
              onChange={e => setEmail(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label>Senha</Label>
            <OutlinedInput
              type="password"
              onChange={e => setPassword(e.target.value)}
            />
          </FormGroup>
          <PasswordReset>Esqueci minha senha</PasswordReset>
          <LoginButton type="button" onClick={() => UserService.signIn(email, password)}>
            Entrar
          </LoginButton>
        </LoginForm>
      </FormColumn>
      <AdColumn>
        <Jumbotron>Bem vindo!</Jumbotron>
      </AdColumn>
    </Container>
  )
}

export default SignIn

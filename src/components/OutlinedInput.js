import React from 'react'
import styled from 'styled-components'

const OutlinedInput = styled.input`
  border: none;
  border-bottom: 1px solid;
  display: block;
  outline: 0;
  padding: 8px 0px;
  border-color: #ccd2d8;
  width: 75%;
  margin-bottom: 16px;
  transition: 0.5s ease-out;
  color: #75ffe3;
  &:focus {
    border-bottom: 1px solid #75ffe3;
  }
`

export default OutlinedInput

import api from './api'

export const getAll = () => {
    return api.get('/patients')
    .then(response => response.data)
}

export const get = (id) => {
    return api.get(`/patients/${id}`)
    .then(response => response.data)
}

export const post = (params) => {
    return api.post(`/patients`, {
        params
    })
}

export default {get, getAll, post}
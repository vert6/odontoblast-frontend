import api from './api'

const signIn = (email, password) => {
  return api.post('/signin', {
    params: {
      email,
      password
    }
  })
}

const signUp = user => {
  const {
    first_name,
    last_name,
    email,
    number,
    password,
    password_confirmation
  } = user

  return api.post('/signup', {
    params: {
      first_name,
      last_name,
      email,
      number,
      password,
      password_confirmation
    }
  })
}

export default { signIn, signUp }
